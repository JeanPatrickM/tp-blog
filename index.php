<!DOCTYPE>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="style.css" />
		<title>Mon super blog !</title>
	</head>

	<body>
		<h1>Mon super blog !</h1>
		<p>Derniers billets du blog :</p>

<?php

try
{
    $bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
}
catch (Exception $e)
{
    die('Erreur : ' . $e->getMessage());
}

$messagesParPage=5; //Nous allons afficher 5 messages par page.

$retour_total=$bdd->query('SELECT COUNT(*) AS total FROM billets'); //Nous récupérons le contenu de la requête dans $retour_total
$donnees_total=$retour_total->fetch(); //On range retour sous la forme d'un tableau.
$total=$donnees_total['total']; //On récupère le total pour le placer dans la variable $total.
 
//Nous allons maintenant compter le nombre de pages.
$nombreDePages=ceil($total/$messagesParPage);
 
if(isset($_GET['page'])) // Si la variable $_GET['page'] existe...
{
     $pageActuelle=intval($_GET['page']);
 
     if($pageActuelle>$nombreDePages) // Si la valeur de $pageActuelle (le numéro de la page) est plus grande que $nombreDePages...
     {
          $pageActuelle=$nombreDePages;
     }
}
else // Sinon
{
     $pageActuelle=1; // La page actuelle est la n°1    
}

$premiereEntree=($pageActuelle-1)*$messagesParPage; // On calcul la première entrée à lire

$retour_messages=$bdd->query('SELECT id, titre, contenu, DATE_FORMAT(date_creation, \'%d/%m/%Y à %Hh%imin%ss\') AS date_creation_fr FROM billets ORDER BY date_creation DESC LIMIT '.$premiereEntree.', '.$messagesParPage.''); 

while ($donnees_messages=$retour_messages->fetch()) // On lit les entrées une à une grâce à une boucle
{
     //Je vais afficher les messages dans des petits tableaux. C'est à vous d'adapter pour votre design...
     //De plus j'ajoute aussi un nl2br pour prendre en compte les sauts à la ligne dans le message.
    echo '<div class="news">
			<h3>' . htmlspecialchars($donnees_messages['titre']) . ' <em>le ' . htmlspecialchars($donnees_messages['date_creation_fr']) . '</em>
			</h3>
			<p>' . nl2br(htmlspecialchars($donnees_messages['contenu'])) . '</br>
				<a href="commentaires.php?billet=' . $donnees_messages['id'] . '"><em>Commentaires</em></a>
			</p>
		</div>';
}

$retour_messages->closeCursor();

echo '<p align="center"> Page : '; //Pour l'affichage, on centre la liste des pages

for($i=1; $i<=$nombreDePages; $i++) //On fait notre boucle
{
     //On va faire notre condition
     if($i==$pageActuelle) //S'il s'agit de la page actuelle...
     {
         echo ' [ '.$i.' ] '; 
     }    
     else //Sinon...
     {
          echo ' <a href="index.php?page='.$i.'">'.$i.'</a> ';
     }
}

echo '</p>';
?>

	</body>
</html>
