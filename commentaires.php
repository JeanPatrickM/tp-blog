<!DOCTYPE>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="style.css" />
		<title>Connexion</title>
	</head>

	<body>
		<h1>Mon super blog !</h1>
		<a href="index.php">Retour à la liste des billets</a>

<?php

try
{
    $bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
}
catch (Exception $e)
{
    die('Erreur : ' . $e->getMessage());
}

if(isset($_GET['billet']) AND $_GET['billet'] > 0) // Si la variable $_GET['billet'] existe...
{
	$id_billet=intval($_GET['billet']);

	$retour_messages_billet=$bdd->prepare('SELECT titre, contenu, DATE_FORMAT(date_creation, \'%d/%m/%Y à %Hh%imin%ss\') AS date_creation_fr FROM billets WHERE id= ?');
	$retour_messages_billet->execute(array($_GET['billet']));
	$donnees_messages_billet=$retour_messages_billet->fetch();
	
	if (!empty($donnees_messages_billet))
	{
		echo '<div class="news">
			<h3>' . htmlspecialchars($donnees_messages_billet['titre']) . ' <em>le ' . htmlspecialchars($donnees_messages_billet['date_creation_fr']) . '</em>
			</h3>
			<p>' . nl2br(htmlspecialchars($donnees_messages_billet['contenu'])) . '
			</p>
		</div>';
	}
	else 
	{
		echo '<p>Ce billet n\'existe pas.</p>';
	}

	$retour_messages_billet->closeCursor();

	echo '<h2>Commentaires</h2>';

	$retour_messages_commentaires=$bdd->prepare('SELECT auteur, commentaire, DATE_FORMAT(date_commentaire, \'%d/%m/%Y à %Hh%imin%ss\') AS date_commentaire_fr FROM commentaires WHERE id_billet= ? ORDER BY date_commentaire');
	$retour_messages_commentaires->execute(array($_GET['billet']));

	while ($donnees_messages_commentaires=$retour_messages_commentaires->fetch())
	{
		echo '<p><strong>' . htmlspecialchars($donnees_messages_commentaires['auteur']) . '</strong> <em>le ' . htmlspecialchars($donnees_messages_commentaires['date_commentaire_fr']) . '</em>
			</p>
			<p>' . nl2br(htmlspecialchars($donnees_messages_commentaires['commentaire'])) . '
			</p>';
	}

$retour_messages_commentaires->closeCursor();

}
else
{
	echo '<p>Veuillez préciser de quel billet vous souhaitez les commentaires.</p>';
}

?>

	</body>
</html>